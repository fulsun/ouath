package pers.fulsun.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorizationServerApplication5 {

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationServerApplication5.class, args);
    }

}