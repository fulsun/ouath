# **[ouath](https://gitee.com/fulsun/ouath)**

- 在 Spring Security 基础上实现 OAuth2.0 实现**授权**的功能。
- 文档 [Spring Security OAuth2.0实现 | 富良的站点 (hexo-blog-fulsun.vercel.app)](https://hexo-blog-fulsun.vercel.app/p/8d8a83c6/)

## OAuth 2.0 

阮一峰提供了几篇关于 OAuth2.0 非常不错的文章，推荐去从瞅瞅。

- [《理解 OAuth2.0》](http://www.iocoder.cn/Fight/ruanyifeng-oauth_2_0/?self)
- [《OAuth2.0 的一个简单解释》](http://www.iocoder.cn/Fight/ruanyifeng-oauth_design/?self)
- [《OAuth2.0 的四种方式》](http://www.iocoder.cn/Fight/ruanyifeng-oauth-grant-types/?self)
- [《GitHub OAuth 第三方登录示例教程》](http://www.iocoder.cn/Fight/ruanyifeng-github-oauth/?self)

## 授权模式

- 客户端必须得到用户的授权（Authorization Grant），才能获得访问令牌（Access Token）。

- OAuth2.0 定义了四种授权方式：

  - 授权码模式（Authorization Code）
  - 密码模式（Resource Owner Password Credentials）
  - 简化模式（Implicit）
  - 客户端模式（Client Credentials）

  - 其中，**密码模式**和**授权码模式**比较常用。至于如何选择，后续慢慢细品。

