package pers.fulsun.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthorizationServerApplication2 {

    public static void main(String[] args) {
        SpringApplication.run(AuthorizationServerApplication2.class, args);
    }

}