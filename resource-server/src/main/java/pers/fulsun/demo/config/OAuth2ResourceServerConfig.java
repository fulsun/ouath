package pers.fulsun.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer //声明开启 OAuth资源服务器的功能
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // 设置 /login 接口无需权限访问，其它接口认证后可访问。
        http.authorizeRequests()
                // 设置 /login 无需权限访问
                .antMatchers("/login").permitAll()
                // 设置 /callback回调地址无需权限验证
                .antMatchers("/callback").permitAll()
                .antMatchers("/callback2").permitAll()
                .antMatchers("/client-login").permitAll()
                // 设置其它请求，需要认证后访问
                .anyRequest().authenticated();
    }

}