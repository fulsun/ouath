package pers.fulsun.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Callback2Controller {

    @GetMapping("/callback2")
    public String login() {
        return "假装这里有一个页面";
    }

}